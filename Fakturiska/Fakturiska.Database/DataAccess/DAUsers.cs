﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Database.DataAccess
{
    public class DAUsers
    {
        public static List<User> GetAllUsers()
        {
            List<User> list = new List<User>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();
            }

            return list;
        }

        public static User GetUser(int id)
        {
            List<User> list = new List<User>();
            User ReturnUser = new User();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();

                var sql = (from user in list
                           where user.UsersID == id
                           select user).ToList();

                if (sql != null)
                {
                    ReturnUser = sql.FirstOrDefault();
                    return ReturnUser;
                }
            }

            return null;
        }

        public static User GetUser(string email)
        {
            List<User> list = new List<User>();
            User ReturnUser = new User();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();

                var sql = (from user in list
                           where user.Email == email
                           select user).ToList();

                if (sql != null)
                {
                    ReturnUser = sql.FirstOrDefault();
                    return ReturnUser;
                }
            }

            return null;
        }

        public static User GetUser(string email, string pass)
        {
            List<User> list = new List<User>();
            User ReturnUser = new User();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();

                var sql = (from user in list
                           where user.Email == email && user.Password == pass
                           select user).ToList();

                if (sql != null)
                {
                    ReturnUser = sql.FirstOrDefault();
                    return ReturnUser;
                }
            }

            return null;
        }

        public static bool Exist(string address)
        {
            List<User> list = new List<User>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();

                var sql = (from user in list
                           where user.Email == address
                           select user).ToList();

                if (sql != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void CreateDB(User newUser)
        {
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                entities.Users.Add(newUser);

                entities.SaveChanges();
            }
        }

        public static void UpdateDB(User newUser)
        {

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                foreach (User sql in entities.Users)
                {
                    if (sql != null)
                    {
                        if (sql.GUID.Equals(newUser.GUID))
                        {
                            sql.Email = newUser.Email;
                            sql.Password = newUser.Password;
                            sql.RoleId = newUser.RoleId;
                            sql.DeleteDate = newUser.DeleteDate;
                            sql.GUID = newUser.GUID;
                        }
                    }
                    else
                    {
                        throw new Exception("Ne radi update User-a!!");
                    }
                }
                entities.SaveChanges();


            }
        }

        public static int GetUserId(string mail)
        {
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from user in entities.Users
                           where user.Email == mail
                           select user.UsersID).FirstOrDefault();

                if(sql>0)
                {
                    return sql;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static User GetUserWithGUID(string uid)
        {
            List<User> list = new List<User>();
            User ReturnUser = new User();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();

                var sql = (from user in list
                           where user.GUID == Guid.Parse(uid)
                           select user).ToList();

                if (sql != null)
                {
                    ReturnUser = sql.FirstOrDefault();
                    return ReturnUser;
                }
            }

            return null;
        }

        public static User GetFirstAdmin()
        {
            List<User> list = new List<User>();
            User ReturnUser = new User();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Users.ToList();

                var sql = (from user in list
                           where user.RoleId == 1
                           select user).ToList();

                if (sql != null)
                {
                    ReturnUser = sql.FirstOrDefault();
                    return ReturnUser;
                }
            }

            return null;
        }

        public static void ChangePassword(string name, string newPassword)
        {
            User ReturnUser = new User();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from user in entities.Users
                           where user.Email == name
                           select user).FirstOrDefault();

                if (sql != null)
                {
                    sql.Password = newPassword;
                    entities.SaveChanges();
                }
            }
        }
    }
}
