﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Fakturiska.Startup))]
namespace Fakturiska
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
