﻿using Fakturiska.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Business.Views
{
    public class LegalPersonsBusinessView
    {
        public int LegalPersonsID { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Fax Number")]
        public string FaxNumber { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Personal Number")]
        public string PersonalNumber { get; set; }

        [Display(Name = "PIB")]
        public string PIB { get; set; }

        [Display(Name = "MIB")]
        public string MIB { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Bank Code")]
        public string BankCode { get; set; }
        public DateTime? DeleteDate { get; set; }
        public System.Guid GUID { get; set; }

        public List<InvoiceBusinessView> RecieverInvoices { get; set; }
        public List<InvoiceBusinessView> PayerInvoices { get; set; }


        public LegalPersonsBusinessView()
        {

        }

        public LegalPersonsBusinessView(LegalPerson LegalPerson, bool AddList = true)
        {
            this.LegalPersonsID = LegalPerson.LegalPersonsID;
            this.Name = LegalPerson.Name;
            this.PhoneNumber = LegalPerson.PhoneNumber;
            this.FaxNumber = LegalPerson.FaxNumber;
            this.Address = LegalPerson.Address;
            this.Website = LegalPerson.Website;
            this.Email = LegalPerson.Email;
            this.PersonalNumber = LegalPerson.PersonalNumber;
            this.PIB = LegalPerson.PIB;
            this.MIB = LegalPerson.MIB;
            this.AccountNumber = LegalPerson.AccountNumber;
            this.BankCode = LegalPerson.BankCode;
            this.DeleteDate = LegalPerson.DeleteDate;
            this.GUID = LegalPerson.GUID;

            if (AddList)
            {
                RecieverInvoices = new List<InvoiceBusinessView>();
                PayerInvoices = new List<InvoiceBusinessView>();

                List<InvoiceBusinessView> invoices = InvoiceBusinessView.GetAllInvoices();
                foreach (InvoiceBusinessView invoice in invoices)
                {
                    if (invoice.RecieverId == this.LegalPersonsID)
                    {
                        RecieverInvoices.Add(invoice);
                    }
                    else if (invoice.PayerId == this.LegalPersonsID)
                    {
                        PayerInvoices.Add(invoice);
                    }
                }
            }
        }

        public static bool CheckIfExist(string pib1)
        {
            return Database.DataAccess.DALegalPersons.CheckIfExist(pib1);
        }

        public static LegalPersonsBusinessView ReturnNewToSave(Database.LegalPerson LegalPerson)
        {
            LegalPersonsBusinessView ReturnLegal = new LegalPersonsBusinessView();

            ReturnLegal.LegalPersonsID = LegalPerson.LegalPersonsID;
            ReturnLegal.Name = LegalPerson.Name;
            ReturnLegal.PhoneNumber = LegalPerson.PhoneNumber;
            ReturnLegal.FaxNumber = LegalPerson.FaxNumber;
            ReturnLegal.Address = LegalPerson.Address;
            ReturnLegal.Website = LegalPerson.Website;
            ReturnLegal.Email = LegalPerson.Email;
            ReturnLegal.PersonalNumber = LegalPerson.PersonalNumber;
            ReturnLegal.PIB = LegalPerson.PIB;
            ReturnLegal.MIB = LegalPerson.MIB;
            ReturnLegal.AccountNumber = LegalPerson.AccountNumber;
            ReturnLegal.BankCode = LegalPerson.BankCode;
            ReturnLegal.DeleteDate = LegalPerson.DeleteDate;
            ReturnLegal.GUID = LegalPerson.GUID;

            return ReturnLegal;
        }

        public static Database.LegalPerson ReturnToLegalPerson(LegalPersonsBusinessView legal)
        {
            Database.LegalPerson ReturnLegalPerson = new LegalPerson();

            ReturnLegalPerson.LegalPersonsID = legal.LegalPersonsID;
            ReturnLegalPerson.Name = legal.Name;
            ReturnLegalPerson.PhoneNumber = legal.PhoneNumber;
            ReturnLegalPerson.FaxNumber = legal.FaxNumber;
            ReturnLegalPerson.Address = legal.Address;
            ReturnLegalPerson.Website = legal.Website;
            ReturnLegalPerson.Email = legal.Email;
            ReturnLegalPerson.PersonalNumber = legal.PersonalNumber;
            ReturnLegalPerson.PIB = legal.PIB;
            ReturnLegalPerson.MIB = legal.MIB;
            ReturnLegalPerson.AccountNumber = legal.AccountNumber;
            ReturnLegalPerson.BankCode = legal.BankCode;
            ReturnLegalPerson.DeleteDate = legal.DeleteDate;
            ReturnLegalPerson.GUID = legal.GUID;

            return ReturnLegalPerson;
        }
        
        public static object GetJSONLegalPerson(string prefix)
        {
            return Database.DataAccess.DALegalPersons.GetJSONLegalPerson(prefix);
        }

        public static void UpdateLegalPersonPIB(LegalPersonsBusinessView legalPersonToSave)
        {
            Database.LegalPerson NewLegalPerson = LegalPersonsBusinessView.ReturnToLegalPerson(legalPersonToSave);

            Database.DataAccess.DALegalPersons.UpdateDBPIB(NewLegalPerson);
        }

        public static void UpdateLegalPerson(LegalPersonsBusinessView legalPersonToSave)
        {
            Database.LegalPerson NewLegalPerson = LegalPersonsBusinessView.ReturnToLegalPerson(legalPersonToSave);

            Database.DataAccess.DALegalPersons.UpdateDB(NewLegalPerson);
        }

        public static List<string> GetAllMIBs()
        {
            return Database.DataAccess.DALegalPersons.GetAllMIBs();
        }

        public static List<string> GetAllPIBs()
        {
            return Database.DataAccess.DALegalPersons.GetALLPIBs();
        }

        public static List<LegalPersonsBusinessView> GetAllLegalPersons1()
        {
            List<LegalPerson> DatabaseLegalPersons = Database.DataAccess.DALegalPersons.GetAllLegalPersons();
            var BusinessLegalPersons = DatabaseLegalPersons.Select(item => new LegalPersonsBusinessView(item, false)).ToList();

            return BusinessLegalPersons;
        }

        internal static bool checkLegalPerson(LegalPersonsBusinessView legalPersonToSave)
        {
            Database.LegalPerson legalPerson = LegalPersonsBusinessView.ReturnToLegalPerson(legalPersonToSave);
            return Database.DataAccess.DALegalPersons.CheckLegalPerson(legalPerson);
        }

        public static bool CheckLegalPerson(LegalPerson legalPerson)
        {
            return Database.DataAccess.DALegalPersons.CheckLegalPerson(legalPerson);
        }

        public static List<LegalPersonsBusinessView> GetAllLegalPersons()
        {
            List<LegalPerson> databaseLegalPersons = Database.DataAccess.DALegalPersons.GetAllLegalPersons();
            var businessLegalPersons = databaseLegalPersons.Select(item => new LegalPersonsBusinessView(item)).ToList();

            return businessLegalPersons;
        }

        public static LegalPersonsBusinessView GetReceiverWithGuidFromAccess(InvoiceBusinessView legalPerson)
        {
            return LegalPersonsBusinessView.GetLegalPersonWithGuid(legalPerson.LegalPerson.GUID.ToString());
        }

        public static LegalPersonsBusinessView GetLegalPerson(int id)
        {
            LegalPersonsBusinessView ReturnLegalPerson = new LegalPersonsBusinessView(Database.DataAccess.DALegalPersons.GetLegalPerson(id));

            return ReturnLegalPerson;
        }

        public static LegalPersonsBusinessView GetLegalPerson(string Pib)
        {
            LegalPersonsBusinessView ReturnLegalPerson = new LegalPersonsBusinessView(Database.DataAccess.DALegalPersons.GetLegalPerson(Pib));

            return ReturnLegalPerson;
        }

        public static void CreateDB(LegalPersonsBusinessView legalPerson)
        {
            Database.LegalPerson NewLegalPerson = new LegalPerson();
            NewLegalPerson = LegalPersonsBusinessView.ReturnToLegalPerson(legalPerson);
            NewLegalPerson.GUID = Guid.NewGuid();
            NewLegalPerson.DeleteDate = null;

            Database.DataAccess.DALegalPersons.CreateLegalPerson(NewLegalPerson);
        }

        public static List<string> GetAllNames()
        {
            return Database.DataAccess.DALegalPersons.GetAllNames();
        }

        public static LegalPersonsBusinessView GetLegalPersonWithGuid(string guid)
        {
            LegalPersonsBusinessView ReturnLegalPerson = new LegalPersonsBusinessView(Database.DataAccess.DALegalPersons.GetLegalPersonWithGuid(guid));

            return ReturnLegalPerson;
        }

    }

}
