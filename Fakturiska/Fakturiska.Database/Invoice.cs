//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Fakturiska.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Invoice
    {
        public int InvoicesID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Sum { get; set; }
        public Nullable<int> InvoiceEstimate { get; set; }
        public Nullable<int> InvoiceTotal { get; set; }
        public Nullable<int> Incoming { get; set; }
        public Nullable<int> Paid { get; set; }
        public Nullable<int> Risk { get; set; }
        public Nullable<int> PriorityId { get; set; }
        public Nullable<int> RecieverId { get; set; }
        public Nullable<int> PayerId { get; set; }
        public string FilePath { get; set; }
        public int UserID { get; set; }
        public Nullable<int> Archive { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
        public System.Guid GUID { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
    
        public virtual LegalPerson LegalPerson { get; set; }
        public virtual Priority Priority { get; set; }
        public virtual LegalPerson LegalPerson1 { get; set; }
        public virtual User User { get; set; }
    }
}
