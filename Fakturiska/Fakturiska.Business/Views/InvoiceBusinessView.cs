﻿using Fakturiska.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Fakturiska.Business.Views
{
    public class InvoiceBusinessView
    {
        public int InvoicesID { get; set; }

        [Display(Name = "Creation Date")]
        public DateTime? Date { get; set; }

        [Display(Name = "Sum")]
        public int? Sum { get; set; }

        [Display(Name = "Invoice Estimate")]
        public bool InvoiceEstimate { get; set; }

        [Display(Name = "Invoice Total")]
        public bool InvoiceTotal { get; set; }

        [Display(Name = "Incoming")]
        public bool Incoming { get; set; }

        [Display(Name = "Paid")]
        public bool Paid { get; set; }

        [Display(Name = "Risk")]
        public bool Risk { get; set; }

        [Display(Name = "Priority")]
        public Nullable<int> PriorityId { get; set; }

        [Display(Name = "Receiver")]
        public Nullable<int> RecieverId { get; set; }

        [Display(Name = "Payer")]
        public Nullable<int> PayerId { get; set; }

        [Display(Name = "Payment Date")]
        public DateTime? PaymentDate { get; set; }

        [Required]
        public string FilePath { get; set; }
        [Display(Name = "User")]
        public int UserID { get; set; }

        internal static void AchiveInvoice(string guid)
        {
            Database.DataAccess.DAInvoices.ArchiveInvoice(guid);
        }

        internal static void DeleteInvoice(string guid)
        {
            Database.DataAccess.DAInvoices.DeleteInvoice(guid);
        }

        public Nullable<int> Archive { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
        public System.Guid GUID { get; set; }

        public virtual LegalPerson LegalPerson { get; set; }
        public string RecieverName { get; set; }
        public virtual Priority Priority { get; set; }
        public virtual LegalPerson LegalPerson1 { get; set; }
        public string PayerName { get; set; }
        public virtual User User { get; set; }

        public string DropFileName { get; set; }
        public string DropFile { get; set; }
        public string DropFileType { get; set; }

        public InvoiceBusinessView()
        {

        }
        public InvoiceBusinessView(Invoice invoice)
        {
            this.InvoicesID = invoice.InvoicesID;
            this.Date = invoice.Date;
            this.Sum = invoice.Sum;
            this.InvoiceEstimate = (invoice.InvoiceEstimate==1)?true:false;
            this.InvoiceTotal = (invoice.InvoiceTotal == 1) ? true:false;
            this.Incoming = (invoice.Incoming==1)? true : false;
            this.Paid =(invoice.Paid==1)? true : false;
            this.Risk = (invoice.Risk==1)? true : false;
            this.PriorityId = invoice.PriorityId;
            this.RecieverId = invoice.RecieverId;
            this.PayerId = invoice.PayerId;
            this.FilePath = invoice.FilePath;
            this.UserID = invoice.UserID;
            this.Archive = invoice.Archive;
            this.DeleteDate = invoice.DeleteDate;
            this.GUID = invoice.GUID;
            if (invoice.PriorityId == null || invoice.PriorityId == 0)
            {
                this.Priority = null;
            }
            else
            {
                this.Priority = Database.DataAccess.DAPriorities.GetPriority(this.PriorityId);
            }
            if (invoice.RecieverId == null || invoice.RecieverId == 0)
            {
                this.LegalPerson = null;
                this.RecieverName = "";
            }
            else
            {
                this.LegalPerson = Database.DataAccess.DALegalPersons.GetLegalPerson(this.RecieverId);
                this.RecieverName = this.LegalPerson.Name;
            }
            if (invoice.PayerId == null || invoice.PayerId == 0)
            {
                this.LegalPerson1 = null;
                this.PayerName = "";
            }
            else
            {
                this.LegalPerson1 = Database.DataAccess.DALegalPersons.GetLegalPerson(this.PayerId);
                this.PayerName = this.LegalPerson1.Name;
            }
            this.User = Database.DataAccess.DAUsers.GetUser(this.UserID);
            this.PaymentDate = invoice.PaymentDate;
      
        }

        internal static void CreateInvoice(InvoiceBusinessView invoice, bool legalPerson1NoExists, bool legalPerson2NoExists)
        {
            Database.Invoice NewInvoice = new Database.Invoice();
            Database.LegalPerson NewLegalPerson1 = null;
            Database.LegalPerson NewLegalPerson2 = null;

            if (legalPerson1NoExists)
            {
                NewLegalPerson1 = invoice.LegalPerson;
            }
            if(legalPerson2NoExists)
            {
                NewLegalPerson2 = invoice.LegalPerson1;
            }

            NewInvoice = InvoiceBusinessView.ReturnToInvoice(invoice);
            NewInvoice.Archive = 0;
            if(NewInvoice.GUID==null || NewInvoice.GUID.Equals(""))
                NewInvoice.GUID = Guid.NewGuid();
            Database.DataAccess.DAInvoices.CreateInvoice(NewInvoice, NewLegalPerson1, NewLegalPerson2);
        }

        public static Database.Invoice ReturnToInvoice(InvoiceBusinessView invoice)
        {
            Database.Invoice ReturnInvoice = new Database.Invoice();

            ReturnInvoice.InvoicesID = invoice.InvoicesID;
            ReturnInvoice.Archive = invoice.Archive;
            ReturnInvoice.Date = invoice.Date;
            ReturnInvoice.DeleteDate = invoice.DeleteDate;
            ReturnInvoice.FilePath = invoice.FilePath;
            ReturnInvoice.GUID = invoice.GUID;
            ReturnInvoice.Incoming = (invoice.Incoming)?1:0;
            ReturnInvoice.InvoiceEstimate = (invoice.InvoiceEstimate)?1:0;
            ReturnInvoice.InvoiceTotal = (invoice.InvoiceTotal)?1:0;
            ReturnInvoice.Paid = (invoice.Paid)?1:0;
            ReturnInvoice.PayerId = invoice.PayerId;
            ReturnInvoice.PriorityId = invoice.PriorityId;
            ReturnInvoice.RecieverId = invoice.RecieverId;
            ReturnInvoice.Risk = (invoice.Risk)?1:0;
            ReturnInvoice.Sum = invoice.Sum;
            ReturnInvoice.UserID = invoice.UserID;
            ReturnInvoice.PaymentDate = invoice.PaymentDate;

            return ReturnInvoice;
        }

        public static List<InvoiceBusinessView> GetAllInvoices()
        {
            List<InvoiceBusinessView> BusinessInvoices = new List<InvoiceBusinessView>();
            List<Invoice> DatabaseInvoices = Database.DataAccess.DAInvoices.GetAllInvoices();

            foreach (Invoice invoice in DatabaseInvoices)
            {
                InvoiceBusinessView NewInvoice = new InvoiceBusinessView(invoice);
                BusinessInvoices.Add(NewInvoice);
            }

            return BusinessInvoices;
        }

        public static InvoiceBusinessView GetInvoice(int id)
        {
            InvoiceBusinessView ReturnInvoice = new InvoiceBusinessView(Database.DataAccess.DAInvoices.GetInvoice(id));

            return ReturnInvoice;
        }
        
        public static void UpdateInvoice(InvoiceBusinessView Invoice)
        {
            Database.LegalPerson legal1 = null;
            Database.LegalPerson legal2 = null;

            if (Invoice.LegalPerson != null)
            {
                Guid guid = Invoice.LegalPerson.GUID;
                if (!guid.Equals(Guid.Empty))
                {
                    if (LegalPersonsBusinessView.CheckLegalPerson((Invoice.LegalPerson)))
                    {
                        LegalPersonsBusinessView legal = LegalPersonsBusinessView.GetLegalPersonWithGuid(Invoice.LegalPerson.GUID.ToString());
                        Database.LegalPerson newLegal = LegalPersonsBusinessView.ReturnToLegalPerson(legal);
                        Invoice.LegalPerson = newLegal;
                        Invoice.RecieverId = newLegal.LegalPersonsID;
                    }
                    else
                    {
                        throw new Exception("Changing Name, PIB or MIB of existing Receiver is not allowed!");
                    }
                }
                else
                {
                    if (Invoice.LegalPerson.Name != null && Invoice.LegalPerson.PIB != null && Invoice.LegalPerson.MIB!=null)
                    {
                        legal1 = Invoice.LegalPerson;
                    }
                    else
                    {
                        Invoice.RecieverId = null;
                    }
                }
            }
            else
            {
                Invoice.RecieverId = null;
            }

            if (Invoice.LegalPerson1 != null)
            {
                if (!Invoice.LegalPerson1.GUID.Equals(Guid.Empty))
                {
                    if (LegalPersonsBusinessView.CheckLegalPerson(Invoice.LegalPerson1))
                    {
                        LegalPersonsBusinessView legal = LegalPersonsBusinessView.GetLegalPersonWithGuid(Invoice.LegalPerson1.GUID.ToString());
                        Database.LegalPerson newLegal = LegalPersonsBusinessView.ReturnToLegalPerson(legal);
                        Invoice.LegalPerson1 = newLegal;
                        Invoice.PayerId = newLegal.LegalPersonsID;
                    }
                    else
                    {
                        throw new Exception("Changing Name, PIB or MIB of existing Payer is not allowed!");
                    }
                }
                else
                {
                    if (Invoice.LegalPerson1.Name != null && Invoice.LegalPerson1.PIB != null && Invoice.LegalPerson1.MIB!=null)
                    {
                        legal2 = Invoice.LegalPerson1;
                    }
                    else
                    {
                        Invoice.PayerId = null;
                    }
                }
            }
            else
            {
                Invoice.PayerId = null;
            }

            Database.Invoice NewInvoice = InvoiceBusinessView.ReturnToInvoice(Invoice);

            Database.DataAccess.DAInvoices.UpdateInvoice(NewInvoice, legal1, legal2);
        }

        public static InvoiceBusinessView GetInvoiceWithGuid(string guid)
        {
            InvoiceBusinessView ReturnInvoice = new InvoiceBusinessView(Database.DataAccess.DAInvoices.GetInvoiceWithGuid(guid));

            return ReturnInvoice;
        }
    }

}
