﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Fakturiska.Controllers
{
    [Authorize]
    public class InvoicesController : Controller
    {
        public static string _user = null;
        

        // GET: Invoices
        public ActionResult Invoices()
        {
            Business.BusinessAccess BusinessAccess = new Business.BusinessAccess();
            List<Business.Views.InvoiceBusinessView> model1 = BusinessAccess.GetAllInvoices();
            List<Business.Views.InvoiceBusinessView> model = new List<Business.Views.InvoiceBusinessView>();
            foreach (Business.Views.InvoiceBusinessView invoice in model1)
            {
                if (invoice.DeleteDate == null)
                {
                    model.Add(invoice);
                }
            }

            return View(model);
        }

        public ActionResult ArchiveInvoice(string guid)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
           
            access.AchiveInvoice(guid);

            return Json(Url.Action("Invoices", "Invoices"));
        }

        public ActionResult DeleteInvoice(string guid)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();

            access.DeleteInvoice(guid);
            return Json(Url.Action("Invoices", "Invoices"));
        }

        public ActionResult CreateInvoice()
        {
            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            List<Business.Views.PrioritiesBusinessView> priorities = BusinesAccess.GetAllPriorities();

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "",
                Value = null
            });
            foreach (Business.Views.PrioritiesBusinessView p in priorities)
            {
                items.Add(new SelectListItem
                {
                    Text = p.Description,
                    Value = p.PrioritiesID.ToString()
                });
            }
            ViewBag.Priorities = items;

            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateInvoice(Business.Views.InvoiceBusinessView invoice, string estimate, string total, string incoming, string paid, string risk)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();

            invoice.InvoiceEstimate = Check(estimate);
            invoice.InvoiceTotal = Check(total);
            invoice.Incoming = Check(incoming);
            invoice.Paid = Check(paid);
            invoice.Risk = Check(risk);
            
            if (User != null)
            {
                Business.Views.UsersBusinessView loggedUser = access.GetUser(User.Identity.Name);
                invoice.UserID = loggedUser.UsersID;
            }
            if (invoice.UserID <= 0)
            {
                return null;
            }
            
            string guid = Guid.NewGuid().ToString();
            string path = "";
            string FileName = guid;
            string filepath = "~/Data/";
            if(invoice.DropFile !=null)
            {
                FileName += invoice.DropFileName;
                byte[] BytesArray = Convert.FromBase64String(invoice.DropFile);

                if(invoice.DropFileType.Equals("image/png"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    MemoryStream memstream = new MemoryStream(BytesArray);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(memstream);
                    img.Save(path);
                }
                else if(invoice.DropFileType.Equals("image/jpeg"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    MemoryStream memstream = new MemoryStream(BytesArray);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(memstream);
                    img.Save(path);
                }
                else if(invoice.DropFileType.Equals("image/jpg"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    MemoryStream memstream = new MemoryStream(BytesArray);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(memstream);
                    img.Save(path);
                }
                else if(invoice.DropFileType.Equals("application/pdf"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    Spire.Pdf.PdfDocument pdfDocument = new Spire.Pdf.PdfDocument(BytesArray);
                    pdfDocument.SaveToFile(path);
                }
                invoice.FilePath = filepath;
                invoice.GUID = Guid.Parse(guid);
            }
            
            if (path.Equals(""))
            {
                if (TempData["FilePath"] == null)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string n = guid + file.FileName;
                    filepath += n;
                    path = Path.Combine(Server.MapPath("~/Data/"), n);
                    string[] name = path.Split('\\');
                    string[] name1 = name.Last().Split('.');

                    if (name1.Last().Equals("pdf") || name1.Last().Equals("jpg") || name1.Last().Equals("png") || name1.Last().Equals("jpeg"))
                    {
                        file.SaveAs(path);
                        invoice.FilePath = filepath;
                        invoice.GUID = Guid.Parse(guid);
                    }
                    else
                    {
                        throw new Exception("File extension is not valid. You can add .pdf, .jpg, .jpeg, .png files only!");
                    }
                }
            }
            if (invoice.Paid)
            {
                invoice.PaymentDate = DateTime.Now;
            }


            access.CreateInvoice(invoice);

            return RedirectToAction("Invoices", "Invoices");

        }

        private bool Check(string val)
        {
            if (val == null) return false;
            else if (val.Equals("")) return false;
            else if (val.Equals("on")) return true;
            else return false;
        }

        public ActionResult EditInvoice(string guid)
        {
            Business.Views.InvoiceBusinessView AllToEdit = Business.Views.InvoiceBusinessView.GetInvoiceWithGuid(guid);
            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            List<Business.Views.PrioritiesBusinessView> priorities = BusinesAccess.GetAllPriorities();

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Text = "",
                Value = null
            });
            foreach (Business.Views.PrioritiesBusinessView p in priorities)
            {
                items.Add(new SelectListItem
                {
                    Text = p.Description,
                    Value = p.PrioritiesID.ToString()
                });
            }
            ViewBag.Priorities = items;
            return View(AllToEdit);
        }

        [HttpPost]
        public ActionResult EditInvoice(Business.Views.InvoiceBusinessView AllToSave, string estimate, string total, string incoming, string paid, string risk)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            AllToSave.InvoiceEstimate = Check(estimate);
            AllToSave.InvoiceTotal = Check(total);
            AllToSave.Incoming = Check(incoming);
            AllToSave.Paid = Check(paid);
            AllToSave.Risk = Check(risk);

            string path = "";
            string FileName = AllToSave.GUID.ToString();
            string filepath = "~/Data/";
            if (AllToSave.DropFile != null)
            {
                FileName += AllToSave.DropFileName;
                byte[] BytesArray = Convert.FromBase64String(AllToSave.DropFile);

                if (AllToSave.DropFileType.Equals("image/png"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    MemoryStream memstream = new MemoryStream(BytesArray);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(memstream);
                    img.Save(path);
                }
                else if (AllToSave.DropFileType.Equals("image/jpeg"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    MemoryStream memstream = new MemoryStream(BytesArray);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(memstream);
                    img.Save(path);
                }
                else if (AllToSave.DropFileType.Equals("image/jpg"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    MemoryStream memstream = new MemoryStream(BytesArray);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(memstream);
                    img.Save(path);
                }
                else if (AllToSave.DropFileType.Equals("application/pdf"))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), FileName);
                    filepath += FileName;
                    Spire.Pdf.PdfDocument pdfDocument = new Spire.Pdf.PdfDocument(BytesArray);
                    pdfDocument.SaveToFile(path);
                }
                AllToSave.FilePath = filepath;
            }
            else
            {
                HttpPostedFileBase file = Request.Files[0];
                if (!file.FileName.Equals(""))
                {
                    path = Path.Combine(Server.MapPath("~/Data/"), Path.GetFileName(file.FileName));
                    filepath += Path.GetFileName(file.FileName);
                    file.SaveAs(path);
                    AllToSave.FilePath = filepath;
                }
            }
            if (AllToSave.Paid)
            {
                AllToSave.PaymentDate = DateTime.Now;
            }
            else
            {
                AllToSave.PaymentDate = null;
            }

            access.UpdateInvoice(AllToSave);

            return RedirectToAction("Invoices", "Invoices");

        }

        public void OpenFile(string path)
        {
            string[] name = path.Split('/');
            string[] name1 = name.Last().Split('.');
            WebClient User = new WebClient();
            Document document = new Document();
            Byte[] FileBuffer;
            if (name1[1].Equals("jpg") || name1[1].Equals("png") || name1[1].Equals("jpeg"))
            {
                using (var stream = new FileStream(Path.Combine(Server.MapPath("~/Data/"), name1[0] + ".pdf"), FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    PdfWriter.GetInstance(document, stream);
                    document.Open();
                    using (var imageStream = new FileStream(Path.Combine(Server.MapPath("~/Data/"), name.Last()), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var image = Image.GetInstance(imageStream);
                        image.SetAbsolutePosition(0, 0); // set the position to bottom left corner of pdf
                        image.ScaleAbsolute(iTextSharp.text.PageSize.A4.Width, iTextSharp.text.PageSize.A4.Height);
                        document.Add(image);
                    }
                    document.Close();
                }

                path = Path.Combine(Server.MapPath("~/Data/"), name1[0] + ".pdf");
            }
            else
            {
                path = Server.MapPath(path);
            }

            FileBuffer = User.DownloadData(path);
            if (FileBuffer != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", FileBuffer.Length.ToString());
                Response.BinaryWrite(FileBuffer);
            }

        }

        [HttpPost]
        public ActionResult Upload()
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            Business.Views.InvoiceBusinessView invoice = new Business.Views.InvoiceBusinessView();

            try
            {
                Business.Views.UsersBusinessView user = access.GetUser(User.Identity.Name);
                invoice.UserID = user.UsersID;
            }
            catch (Exception e)
            {
                throw new Exception("Please log in!");
            }
            string guid = Guid.NewGuid().ToString();
            string file1 = "";
            string filepath = "~/Data/";
            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase files = Request.Files;
                string fname;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                    fname = guid+file.FileName;
                    filepath += fname;
                    fname = Path.Combine(Server.MapPath("~/Data"), fname);
                    file.SaveAs(fname);
                    file1 = filepath;
                }
            }

            invoice.FilePath = file1;
            if (invoice.Paid)
            {
                invoice.PaymentDate = DateTime.Now;
            }


            invoice.GUID = Guid.Parse(guid);
            access.CreateInvoice(invoice);
            return Json(Url.Action("Invoices", "Invoices"));
        }

        
    }
}