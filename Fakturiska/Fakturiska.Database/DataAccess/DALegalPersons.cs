﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Database.DataAccess
{
    public class DALegalPersons
    {
        public static List<LegalPerson> GetAllLegalPersons()
        {
            List<LegalPerson> list = new List<LegalPerson>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.LegalPersons.ToList();
            }

            return list;
        }

        public static LegalPerson GetLegalPerson(int? id)
        {
            List<LegalPerson> list = new List<LegalPerson>();
            LegalPerson ReturnLegalPerson = new LegalPerson();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.LegalPersons.ToList();

                var sql = (from legalPerson in list
                           where legalPerson.LegalPersonsID == id
                           select legalPerson).ToList();

                if (sql != null)
                {
                    ReturnLegalPerson = sql.FirstOrDefault();
                    return ReturnLegalPerson;
                }

            }

            return null;
        }

        public static LegalPerson GetLegalPerson(string Pib)
        {
            List<LegalPerson> list = new List<LegalPerson>();
            LegalPerson ReturnLegalPerson = new LegalPerson();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.LegalPersons.ToList();

                var sql = (from legalPerson in list
                           where legalPerson.PIB == Pib
                           select legalPerson).ToList();

                if (sql != null)
                {
                    ReturnLegalPerson = sql.FirstOrDefault();
                    return ReturnLegalPerson;
                }

            }

            return null;
        }

        public static void CreateLegalPerson(LegalPerson newLegalPerson)
        {
            if (newLegalPerson.GUID==null || newLegalPerson.GUID.Equals(Guid.Empty))
                newLegalPerson.GUID = Guid.NewGuid();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                entities.LegalPersons.Add(newLegalPerson);

                entities.SaveChanges();
            }
        }

        public static void UpdateDB(LegalPerson newLegalPerson)
        {
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from legal in entities.LegalPersons
                           where legal.LegalPersonsID == newLegalPerson.LegalPersonsID
                           select legal).FirstOrDefault();

                if (sql != null)
                {
                    sql.AccountNumber = newLegalPerson.AccountNumber;
                    sql.Address = newLegalPerson.Address;
                    sql.BankCode = newLegalPerson.BankCode;
                    sql.DeleteDate = newLegalPerson.DeleteDate;
                    sql.Email = newLegalPerson.Email;
                    sql.FaxNumber = newLegalPerson.FaxNumber;
                    sql.GUID = newLegalPerson.GUID;
                    sql.LegalPersonsID = newLegalPerson.LegalPersonsID;
                    sql.MIB = newLegalPerson.MIB;
                    sql.Name = newLegalPerson.Name;
                    sql.PersonalNumber = newLegalPerson.PersonalNumber;
                    sql.PhoneNumber = newLegalPerson.PhoneNumber;
                    sql.PIB = newLegalPerson.PIB;
                    sql.Website = newLegalPerson.Website;

                    entities.SaveChanges();
                }

            }
        }

        public static bool CheckIfExist(string pib1)
        {
            bool exist = false;

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from lp in entities.LegalPersons
                           where lp.PIB == pib1
                           select lp).FirstOrDefault();

                if (sql != null)
                {
                    exist = true;
                }
            }

            return exist;
        }

        public static void UpdateDBPIB(LegalPerson newLegalPerson)
        {
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from legal in entities.LegalPersons
                           where legal.PIB == newLegalPerson.PIB
                           select legal).FirstOrDefault();

                if (sql != null)
                {
                    sql.AccountNumber = newLegalPerson.AccountNumber;
                    sql.Address = newLegalPerson.Address;
                    sql.BankCode = newLegalPerson.BankCode;
                    sql.DeleteDate = newLegalPerson.DeleteDate;
                    sql.Email = newLegalPerson.Email;
                    sql.FaxNumber = newLegalPerson.FaxNumber;
                    sql.GUID = newLegalPerson.GUID;
                    sql.MIB = newLegalPerson.MIB;
                    sql.Name = newLegalPerson.Name;
                    sql.PersonalNumber = newLegalPerson.PersonalNumber;
                    sql.PhoneNumber = newLegalPerson.PhoneNumber;
                    sql.PIB = newLegalPerson.PIB;
                    sql.Website = newLegalPerson.Website;

                    entities.SaveChanges();
                }

            }
        }

        public static object GetJSONLegalPerson(string prefix)
        {
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var LegalPersons = (from c in entities.LegalPersons
                                    where c.Name.StartsWith(prefix)
                                    select new { c.Name, c.LegalPersonsID });

                return LegalPersons;
            }

        }

        public static List<string> GetAllNames()
        {
            List<string> list = new List<string>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from lp in entities.LegalPersons
                           where lp.DeleteDate == null
                           select lp.Name);

                if (sql != null)
                {
                    list = sql.ToList();
                }
            }

            return list;
        }

        public static List<string> GetALLPIBs()
        {
            List<string> list = new List<string>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from lp in entities.LegalPersons
                           where lp.DeleteDate == null
                           select lp.PIB);

                if (sql != null)
                {
                    list = sql.ToList();
                }
            }

            return list;
        }
        
        public static bool CheckLegalPerson(LegalPerson legalPerson)
        {
            
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from lp in entities.LegalPersons
                           where lp.GUID==legalPerson.GUID 
                           && lp.Name == legalPerson.Name 
                           && lp.PIB == legalPerson.PIB
                           && lp.MIB == legalPerson.MIB
                           select lp).FirstOrDefault();

                if (sql != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static List<string> GetAllMIBs()
        {
            List<string> list = new List<string>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from lp in entities.LegalPersons
                           where lp.DeleteDate == null
                           select lp.MIB);

                if (sql != null)
                {
                    list = sql.ToList();
                }
            }

            return list;
        }

        public static LegalPerson GetLegalPersonWithGuid(string guid)
        {
            List<LegalPerson> list = new List<LegalPerson>();
            LegalPerson ReturnLegalPerson = new LegalPerson();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.LegalPersons.ToList();

                var sql = (from legalPerson in list
                           where legalPerson.GUID == Guid.Parse(guid)
                           select legalPerson).ToList();

                if (sql != null)
                {
                    ReturnLegalPerson = sql.FirstOrDefault();
                    return ReturnLegalPerson;
                }

            }

            return null;
        }
    }

}
