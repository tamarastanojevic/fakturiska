﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Database.DataAccess
{
    public class DARoles
    {
        public static List<Role> GetAllRoles()
        {
            List<Role> list = new List<Role>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Roles.ToList();
            }

            return list;
        }

        public static Role GetRole(int id)
        {
            List<Role> list = new List<Role>();
            Role ReturnRole = new Role();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Roles.ToList();

                var roles = (from r in list
                             where r.RolesID == id
                             select r).ToList();

                if (roles != null)
                {
                    ReturnRole = roles.FirstOrDefault();
                    return ReturnRole;
                }

            }

            return null;
        }


    }
}
